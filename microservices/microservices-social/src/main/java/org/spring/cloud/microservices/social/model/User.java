package org.spring.cloud.microservices.social.model;


//@JsonInclude(Include.NON_NULL)
public class User extends BaseModel {
	private Integer id;

	private String name;

	private Integer age;

	//添加无参的构造器
	public User(){
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
