/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.spring.cloud.microservices.social.controller;

import org.spring.cloud.microservices.social.feign.UserFeignClient;
import org.spring.cloud.microservices.social.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
//@Api(value = "User",description = "测试")
public class ViewpointController {
//
//	@Autowired
//	private RestTemplate restTemplate;

	@Autowired
	private UserFeignClient userFeignClient;


//	@ApiOperation(value = "查询", notes = "简单接口描述", code = 200, produces = "application/json")
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
//	@ResponseBody
	public User findOne() {
		return userFeignClient.findOne();
//		return this.restTemplate.getForObject("http://microservices-usercenter/findOne", User.class);
	}
//
//
//	@ApiOperation(value = "查询", notes = "简单接口描述", code = 200, produces = "application/json")
//	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
//	@ResponseBody
//	public List<User> findAll() {
//		return this.restTemplate.getForObject("http://microservices-usercenter/findAll", List.class);
//	}

}
