package org.spring.cloud.microservices.social;

import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * Created by moyin18602 on 2016/11/8.
 */
@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }
}
