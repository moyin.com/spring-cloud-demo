/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.spring.cloud.microservices.usercenter.controller;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.spring.cloud.microservices.usercenter.model.User;
import org.spring.cloud.microservices.usercenter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "User",description = "测试")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private EurekaClient eurekaClient;

	@GetMapping("/eureka-instance")
	public String serviceUrl() {
		InstanceInfo instanceInfo = this.eurekaClient.getNextServerFromEureka("microservices-usercenter",false);
		return instanceInfo.getHomePageUrl();
	}

	@ApiOperation(value = "查询单一记录", notes = "简单接口描述", code = 200, produces = "application/json")
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	public User findOne() {
		return this.userService.findOne(1);
	}

	@ApiOperation(value = "查询", notes = "简单接口描述", code = 200, produces = "application/json")
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	@ResponseBody
	public List<User> findAll() {
		return this.userService.getList();
	}

	@ApiOperation(value = "保存", notes = "简单接口描述 user,age必填", code = 200, produces = "application/json")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Object save(
			@ApiParam(required = true, name = "demo", value = "name, age, x")
			@RequestBody User demo) {
		return userService.save(demo);
	}


	@ApiOperation(value = "删除", notes = "简单接口描述 user,age必填", code = 200, produces = "application/json")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void delete( @RequestBody User demo) {
		this.userService.delete(demo.getId());
	}
}
