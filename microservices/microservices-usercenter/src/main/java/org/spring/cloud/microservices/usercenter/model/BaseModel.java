package org.spring.cloud.microservices.usercenter.model;

import java.io.Serializable;

public class BaseModel implements Serializable {
	Integer x;

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}
}
