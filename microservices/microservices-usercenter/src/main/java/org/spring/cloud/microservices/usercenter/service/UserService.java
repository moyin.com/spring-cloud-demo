package org.spring.cloud.microservices.usercenter.service;



import org.spring.cloud.microservices.usercenter.model.User;

import java.util.List;


/**
 * Created by moyin18602 on 2016/9/3.
 */
public interface UserService {
    User findOne(Integer id);

    List<User> getList();

    public User save(User demo);

    public void delete(Integer id);
}
