package org.spring.cloud.microservices.usercenter.model;


import javax.persistence.*;

//@JsonInclude(Include.NON_NULL)
@Entity
@Table(name="user")
public class User extends BaseModel {
	@Id
	@GeneratedValue
//	@JsonIgnore
	@Column(name="id")
	private Integer id;

	@Column(name="name")
	private String name;

	@Column(name="age")
	private Integer age;

	//添加无参的构造器
	public User(){
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
