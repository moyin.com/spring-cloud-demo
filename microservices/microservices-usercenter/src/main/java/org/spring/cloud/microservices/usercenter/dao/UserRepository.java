package org.spring.cloud.microservices.usercenter.dao;


import org.spring.cloud.microservices.usercenter.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

}
